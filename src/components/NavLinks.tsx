import { Button, ButtonGroup } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";

const NavLinks = () => {
  const location = useLocation();

  useEffect(() => {
    if (location.state) {
      toggleAuthentication();
    }
  }, [location]);

  const [isAuthenticated, setIsAuthenticated] = useState<boolean>(true);

  const toggleAuthentication = () => {
    setIsAuthenticated((prev) => !prev);
  };

  return (
    <ButtonGroup gap={4}>
      {isAuthenticated ? (
        <>
          <Button colorScheme="teal">
            <Link to="/">Home</Link>
          </Button>
          <Button colorScheme="teal">
            <Link to="/about">About</Link>
          </Button>
          <Button colorScheme="teal">
            <Link to="/posts">Posts</Link>
          </Button>
          <Button colorScheme="red" onClick={toggleAuthentication}>
            Logout
          </Button>
        </>
      ) : (
        <>
          <Button colorScheme="twitter">
            <Link to="/register">Register</Link>
          </Button>
          <Button colorScheme="telegram">
            <Link to="/login">Log in</Link>
          </Button>
        </>
      )}
    </ButtonGroup>
  );
};

export default NavLinks;
