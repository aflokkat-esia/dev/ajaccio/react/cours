import { Input, List, ListItem } from "@chakra-ui/react";
import { Fragment, useEffect, useRef, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Post } from "../types";

type Props = {
  posts: Post[];
};

const PostList = (props: Props) => {
  const navigate = useNavigate();

  const { posts } = props;

  const [filteredPosts, setFilteredPosts] = useState<Post[]>([]);

  const searchRef = useRef<HTMLInputElement>(null);

  function onEnterFilterPosts(e: React.KeyboardEvent) {
    if (e.key == "Enter") {
      const tmp = posts.filter((post) =>
        post.title.includes(searchRef.current?.value || "")
      );
      if (tmp.length === 1) {
        navigate(`/posts/${tmp[0].id}`);
      }
      setFilteredPosts(tmp);
    }
  }

  useEffect(() => {
    setFilteredPosts(posts);
  }, [posts]);

  return (
    <>
      <Input
        w={"25%"}
        placeholder="Search in title..."
        m={4}
        ref={searchRef}
        onKeyDown={onEnterFilterPosts}
      />
      <List>
        {filteredPosts.map((post) => (
          <Fragment key={post.id}>
            <ListItem>
              <Link to={`/posts/${post.id}`}>{post.id}</Link> - {post.title}
            </ListItem>
            {/* <ListItem>{post.body}</ListItem> */}
          </Fragment>
        ))}
      </List>
    </>
  );
};

export default PostList;
