import { Button, Flex, Heading, Spacer } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { User } from "../types";
import NavLinks from "./NavLinks";

type Props = {
  user: User;
};

const Navbar = (props: Props) => {
  const { user } = props;

  const [mode, setMode] = useState<string>("light");

  function toggleMode() {
    setMode(mode === "light" ? "dark" : "light");
  }

  useEffect(() => {
    const localStorageMode = localStorage.getItem("mode");
    if (localStorageMode) {
      setMode(localStorageMode);
    } else {
      localStorage.setItem("mode", "light");
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("mode", mode);
  }, [mode]);

  return (
    <Flex
      minWidth="max-content"
      alignItems="center"
      px={6}
      py={4}
      borderBottomColor="teal.700"
      borderBottomWidth={4}
      gap={6}
    >
      <Heading size="lg">AfloApp</Heading>
      <Spacer />
      {/* <Text>Bienvenue, {user.username}</Text> */}
      <NavLinks />
      <Button onClick={toggleMode}>{mode === "light" ? "🌙" : "☀️"}</Button>
    </Flex>
  );
};

export default Navbar;
