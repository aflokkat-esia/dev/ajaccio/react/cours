import { Route, Routes } from "react-router-dom";
import MainLayout from "./layouts/MainLayout";
import About from "./pages/About";
import Home from "./pages/Home";
import Login from "./pages/Login";
import NotFound from "./pages/NotFound";
import PostDetails from "./pages/PostDetails";
import PostNew from "./pages/PostNew";
import Posts from "./pages/Posts";
import Register from "./pages/Register";
import { Hobby } from "./types";

const App = () => {
  const hobbies: Hobby[] = [
    { name: "Sport", isFavorite: true },
    { name: "Musique", isFavorite: true },
    { name: "Programming in Angular with Postman", isFavorite: false },
  ];

  return (
    <Routes>
      <Route path="/" element={<MainLayout />}>
        <Route index element={<Home hobbies={hobbies} />} />
        <Route path="about" element={<About />} />
        <Route path="posts" element={<Posts />} />
        <Route path="posts/:id" element={<PostDetails />} />
        <Route path="posts/new" element={<PostNew />} />
        <Route path="login" element={<Login />} />
        <Route path="register" element={<Register />} />
        <Route path="*" element={<NotFound />} />
      </Route>
      {/* <Navbar user={user} />
      <Container maxW="full" p={4}>
        <Home hobbies={hobbies} />
        <PostList posts={posts} />
      </Container> */}
    </Routes>
  );
};

export default App;
