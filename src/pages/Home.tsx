import {
  Button,
  Heading,
  HStack,
  Input,
  Switch,
  Text,
  VStack,
} from "@chakra-ui/react";
import { ChangeEvent, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { Hobby } from "../types";

type Props = {
  hobbies: Hobby[];
};

const Home = (props: Props) => {
  const { hobbies } = props;

  const location = useLocation();

  const toto = "Toto";
  const color = "green";

  const [count, setCount] = useState<number>(0);
  const [input, setInput] = useState<string>("");
  const [parsedInput, setParsedInput] = useState<number>(0);
  const [switchs, setSwitchs] = useState<boolean>(false);
  const [favorites, setFavorites] = useState<boolean>(false);

  function increment() {
    setCount((prevCount) => prevCount + parsedInput);
    // setCount((prevCount) => {
    //   if (prevCount < 10) {
    //     return prevCount + 1;
    //   } else {
    //     return prevCount;
    //   }
    // });
  }

  function multiplication() {
    setCount((prevCount) => prevCount * parsedInput);
  }

  function handleChangeInput(e: ChangeEvent<HTMLInputElement>) {
    setInput(e.target.value);
  }

  function handleChangeSwitch() {
    setSwitchs((prevSwitch) => !prevSwitch);
  }

  function handleChangeFavorites() {
    setFavorites((prevFavorites) => !prevFavorites);
  }

  function reset() {
    setCount(0);
    setInput("");
    setSwitchs(false);
  }

  useEffect(() => {
    console.log("Toto");
  }, [count, switchs]);

  useEffect(() => {
    const tmp = parseInt(input);
    if (!isNaN(tmp)) setParsedInput(tmp);
    else setParsedInput(0);
  }, [input]);

  return (
    <>
      <VStack align={"stretch"} gap={10}>
        <HStack gap={10}>
          <Heading color={toto === "Toto" ? color : "blue"}>
            Home Page - {count}
          </Heading>
          <Button
            colorScheme={"teal"}
            onClick={switchs ? multiplication : increment}
            isDisabled={!isNaN(parsedInput) ? false : true}
          >
            {parsedInput
              ? switchs
                ? "x" + (parsedInput >= 0 ? input : `(${input})`)
                : (parsedInput >= 0 ? "+" : "") + input
              : "0"}
          </Button>
          <Input
            type="number"
            variant="filled"
            placeholder="Number"
            w={"50%"}
            value={input}
            onChange={handleChangeInput}
          />
        </HStack>
        <HStack>
          <Text>Addition</Text>
          <Switch isChecked={switchs} onChange={handleChangeSwitch} />
          <Text>Multiplication</Text>
        </HStack>
        <HStack>
          {!(count === 0 && input === "" && switchs === false) && (
            <Button onClick={reset}>Reset</Button>
          )}
        </HStack>
        <HStack>
          <Text>Non favories</Text>
          <Switch isChecked={favorites} onChange={handleChangeFavorites} />
          <Text>Favories</Text>
        </HStack>
        {hobbies
          ?.filter((hobby) => hobby.isFavorite === favorites)
          .map((hobby) => (
            <Text key={hobby.name}>{hobby.name}</Text>
          ))}
        {/* {hobbies.map((hobby) => {
          if (hobby.isFavorite) {
            return <Text key={hobby.name}>{hobby.name}</Text>;
          }
        })} */}
      </VStack>
    </>
  );
};

export default Home;
