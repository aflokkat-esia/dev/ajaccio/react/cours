import { Button, Heading, Input, VStack } from "@chakra-ui/react";
import axios from "axios";
import { useRef } from "react";
import { useNavigate } from "react-router-dom";

const PostNew = () => {
  const navigate = useNavigate();

  const titleRef = useRef<any>();
  const bodyRef = useRef<any>();

  function onClickCreatePost() {
    axios
      .post(`https://jsonplaceholder.typicode.com/posts/`, {
        title: titleRef.current.value,
        body: bodyRef.current.value,
        userId: 1,
      })
      .then((res) => {
        console.log(res);
        navigate("/posts");
      })
      .catch((err) => console.log(err));
  }

  return (
    <>
      <VStack spacing={20}>
        <Heading>PostNew</Heading>
        <Input placeholder="Title" ref={titleRef} />
        <Input placeholder="Body" ref={bodyRef} />
        <Button onClick={onClickCreatePost}>Submit</Button>
      </VStack>
    </>
  );
};

export default PostNew;
