import { Heading } from "@chakra-ui/react";
import axios from "axios";
import { useEffect, useState } from "react";
import PostList from "../components/PostList";
import { Post } from "../types";

const Posts = () => {
  const [posts, setPosts] = useState<Post[]>([]);

  useEffect(() => {
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then((res) => setPosts(res.data));
  }, []);

  return (
    <>
      <Heading>Posts</Heading>
      <PostList posts={posts} />
    </>
  );
};

export default Posts;
