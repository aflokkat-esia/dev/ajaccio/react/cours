import { Button, Container, Heading, Input, VStack } from "@chakra-ui/react";
import { Link } from "react-router-dom";

const Login = () => {
  return (
    <Container mt={10}>
      <VStack gap={10}>
        <Heading>Login</Heading>
        <Input placeholder="Username" />
        <Input placeholder="Password" />
        <Button>
          <Link to="/" state={"Coucou"}>
            Submit
          </Link>
        </Button>
      </VStack>
    </Container>
  );
};

export default Login;
