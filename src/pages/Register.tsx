import { Button, Center, Heading, Input, VStack } from "@chakra-ui/react";
import { Link } from "react-router-dom";

const Register = () => {
  return (
    <Center>
      <VStack>
        <Heading>Register</Heading>
        <Input placeholder="Username" />
        <Input placeholder="Email" />
        <Input placeholder="Password" />
        <Input placeholder="Repeat Password" />
        <Button>
          <Link to="/login">Submit</Link>
        </Button>
      </VStack>
    </Center>
  );
};

export default Register;
