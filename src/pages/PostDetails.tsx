import { Button, Heading, Text } from "@chakra-ui/react";
import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Post } from "../types";

const PostDetails = () => {
  const navigate = useNavigate();

  const { id } = useParams();

  const [post, setPost] = useState<Post>();
  const [isFetching, setIsFetching] = useState<boolean>(false);

  useEffect(() => {
    setIsFetching(true);
    axios
      .get(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then((res) => {
        setPost(res.data);
        setIsFetching(false);
      })
      .catch((err) => {
        console.log(err);
        setIsFetching(false);
      });
  }, []);

  return (
    <>
      <Button onClick={() => navigate(-1)}>Retour arrière</Button>
      <Heading>PostDetails</Heading>
      {isFetching ? <Text>Loading...</Text> : <Text>{post?.body}</Text>}
    </>
  );
};

export default PostDetails;
